# -*- coding: utf-8 -*-

""" 
filetools.cache.create_db.py
--------------------

This module creates the SQLite database for RhinoFile analytics.
"""

def create_db(directory : str) -> None:
    """Create a sqlite database for the file cache
    
    Parameters
    ------------
    directory : str, a file-like path to the directory where the database will be created.
    
    """
    import sqlite3
    conn = sqlite3.connect(f'{directory}\FileData.db')
    c = conn.cursor()
    # create the File table
    try:
        c.execute('''
            CREATE TABLE RhinoFiles
            ( 
                id INT PRIMARY KEY,
                filename TEXT NOT NULL,
                filesize REAL NOT NULL,
                project_number INT NOT NULL,
                application_name TEXT NOT NULL,
                application_url TEXT,
                application_details TEXT NOT NULL,
                created_by TEXT NOT NULL,
                last_edited_by TEXT NOT NULL,
                revision INT NOT NULL,
                unit_system TEXT NOT NULL,
                named_view_count INT,
                layer_heirarchy JSON,
                object_count INT,
                instance_definition_count INT
            )
            ''')
        return None
    except Exception as e:
        print(f'Something went wrong while creating the Table: {e}')
        
