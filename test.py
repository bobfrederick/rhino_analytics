import plotly.express as px
df = px.data.tips()
print(df)
fig = px.treemap(df, path=['day', 'time', 'sex'])
fig.update_traces(root_color="lightgrey")
fig.update_layout(margin = dict(t=50, l=25, r=25, b=25))
fig.show()