import dash
from filetools import layers, read3dm
import pandas as pd
import dash_bootstrap_components as dbc
import plotly.express as px

app = dash.Dash(
    external_stylesheets=[dbc.themes.BOOTSTRAP]
)

folder = r'C:\Users\bfrederick\Desktop\ExampleRhinoLayers\BARDAS'
files_data = read3dm.get_multiple_file_data(folder)

files_layers_parsed = []
for file in files_data:
    layer_data = layers.get_layer_data(file.file3dm)
    heirarchy = layers.get_layer_heirarchy(layer_data)
    files_layers_parsed.append(heirarchy)

# testing just the first file. 
file_1 = files_layers_parsed[0]

data = dict(layer = file_1[0], 
            parents = file_1[1]
            )

fig = px.icicle(data,
                names = 'layer', 
                parents = 'parents',
                )

fig.update_traces(root_color="lightgrey")
fig.update_layout(margin = dict(t=50, l=25, r=25, b=25))
fig.show()

"""
if __name__ == '__main__':
    pass
    #app.run_server()
"""