# -*- coding: utf-8 -*-
#                                         
# ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ ____ 
#||r |||h |||i |||n |||o |||_ |||a |||n |||a |||l |||y |||t |||i |||c |||s ||
#||__|||__|||__|||__|||__|||__|||__|||__|||__|||__|||__|||__|||__|||__|||__||
#|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|/__\|      
#

"""
"rhino_analytics" file parsing library
--------------------------

rhino_analytics is currently a tiny file parsing library with 
caching and sqlite support, written in Python 🐍
for users who want to learn more about their Rhino files 
It's mainly convenience functions wrapping rhino3dm.

"""