# -*- coding: utf-8 -*-

""" 
filetools.layerparse
--------------------

This module contains functions to help parse layers in Rhino3dm files.
"""


def get_layer_data(file) -> list[list[str]]:
    """ From a list of File3dm Objects return nested lists of strings for the layer full path.
    
    Parameters
    ----------
    file_data : list of rhino3dm.File3dm Objects
    
    Returns
    -------
    A nested list of strings representing the full path for each layer in the file.
    """
    layer_heirarchy = []
    for layer in file.Layers:
        layer_heirarchy.append(layer.FullPath.split('::'))

    return layer_heirarchy

        
def get_layer_heirarchy(layer_data : list) -> list[tuple]:
    """ Parse Rhino layer full path strings into two lists containing the layer child names and parents
    
    Parameters
    ----------
    layer_data : a nested list of strings representing the full path for each layer in the file.
    
    """
    names = []
    parents = []

    for layer_heirarchy in layer_data:
        if len(layer_heirarchy) == 1:
            names.append(layer_heirarchy[len(layer_heirarchy)-1])
            parents.append("ROOT")
        else:
            names.append(layer_heirarchy[len(layer_heirarchy)-1])
            parents.append(layer_heirarchy[len(layer_heirarchy)-2])

    return (names, parents)


if __name__ == "__main__":
    from read3dm import get_file_data
    file = r"C:\Users\bfrederick\Desktop\ExampleRhinoLayers\BARDAS\1014_Sears_massing-combined DM.3dm"
    folder = r'C:\Users\bfrederick\Desktop\ExampleRhinoLayers\BARDAS'
    file_data = get_file_data(file)
    file_list = list(file_data)
    layer_data = get_layer_data(file_data.file3dm)
    parsed_layers = get_layer_heirarchy(layer_data)
    print(parsed_layers)
    
    
