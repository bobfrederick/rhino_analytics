# -*- coding: utf-8 -*-

""" 
filetools.read3dm
--------------------

This module contains functions to help read Rhino3dm files.
"""

from rhino3dm import File3dm
import os
import pathlib
from collections import namedtuple

FileData = namedtuple('File',['file3dm','filename'])


def get_multiple_file_data(directory:str, recursive=False):
    """ Opens a folder and returns an iterable of child 3dmFile objects. 
    
    Parameters
    -----------
    directory : str like path to a folder containing multiple Rhino3dm files.
    recursive : bool, set to True to retreive rhino files in subfolders.

    """
    if os.path.isdir(directory):
        pass
    else:
        raise ValueError("Directory parameter must be a file-like path string to a valid directory")
    
    if recursive:
        for root, dir, files in os.walk(directory):
            for filename in files:
                if filename.endswith(".3dm"):
                    filepath = os.path.join(root,filename)
                    yield FileData(File3dm.Read(filepath),filepath)

    else:
        for filename in os.listdir(directory):
            if filename.endswith(".3dm"):
                filepath = os.path.join(directory,filename)
                yield FileData(File3dm.Read(filepath), filepath)

def get_file_data(filepath):
    """ Opens a file and returns a 3dmFile object.
    
    Parameters
    ----------
    filepath : string like path to a sigle Rhino3dm file. 
    """
    if os.path.isfile(filepath):
        file = FileData(File3dm.Read(filepath), filepath)
        return file
    else:
        return None

if __name__ == '__main__':
    file_local = r"C:\Users\bfrederick\Desktop\ExampleRhinoLayers\BARDAS\1014_Sears_massing-combined DM.3dm"
    folder_local = r'C:\Users\bfrederick\Desktop\ExampleRhinoLayers\BARDAS'
    folder_server = r'P:\21615 BARDAS Project Oz\21615_000 Models\21615_000_0 Rhino'
    
    # try getting multiple files recursively
    result = get_multiple_file_data(folder_server, recursive=True)
    result = list(result)

    for object in result:
        print(object.file3dm)
        print(object.filename)
    print("Finished. Got Multiple Files...")
    
    # try getting a single file
    result2 = get_file_data(file_local)
    print(result2.file3dm)
    print(result2.filename)
    print("Finished. Got Single File...")
